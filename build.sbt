name := "lab-streams"

version := "1.0"

scalaVersion := "2.11.8"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= {
  Seq(
    "com.typesafe.akka" % "akka-stream_2.11" % "2.4.4",
    "com.typesafe.akka" %% "akka-http-core" % "2.4.4",
    "com.typesafe.akka" %% "akka-http-experimental" % "2.4.4",
    "com.typesafe.akka" %% "akka-http-spray-json-experimental" % "2.4.4"
  )
}
