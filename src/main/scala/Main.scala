
import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.stream._
import akka.stream.scaladsl._
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.{Unmarshal}
import akka.util.{ByteString, Timeout}
import spray.json.{DefaultJsonProtocol, DeserializationException, JsObject, JsString, JsValue, RootJsonFormat}

case class DocSentiment(score: Option[String], typex: String)

case class Sentiment(status: String, usage: String, totalTransactions: String, language: String, docSentiment: DocSentiment)

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val docSentimentFormat = jsonFormat(DocSentiment, "score", "type")
  implicit val sentimentFormat = jsonFormat5(Sentiment)
}

object Main extends App with JsonSupport {

  val ALCHEMY_HOST = "gateway-a.watsonplatform.net"
  val ALCHEMY_RESOURCE = "/calls/text/TextGetTextSentiment"
  val API_KEY = "af5335e69195d6109ee74908a2abe61c1267d97d"

  implicit val system = ActorSystem("lab-stream")
  implicit val ec = system.dispatcher
  implicit val materializer = ActorMaterializer()

  val inStream = () => getClass.getResourceAsStream("/words.txt")
  val inFile = getClass.getResource("/words.txt").getFile

  val source = StreamConverters.fromInputStream(inStream)
  val delimiterFlow = Framing.delimiter(ByteString("\n"), maximumFrameLength = 256, allowTruncation = true)
  val connectionFlow = Http().outgoingConnection(ALCHEMY_HOST)

  //TODO: here is something wrong :)
  val responseFlow = Flow[HttpResponse]
    .mapAsync(1) { response =>
      implicit val timeout = Timeout(1, TimeUnit.SECONDS)
      Unmarshal(response.entity).to[Sentiment]
    }

  println("Test reactive streams")

  val tStart = System.currentTimeMillis()

  val flow = source
    .via(delimiterFlow)
    .map(_.utf8String)
    .map(word => toAlchemyRequest(word))
    .via(connectionFlow)
    .via(responseFlow)
    .map(sentiment => sentiment.docSentiment.typex)
    .runWith(Sink.foreach(res => println(s"result: ${res}")))

  flow.onComplete {
    case _ => {
      val tEnd = System.currentTimeMillis()
      println("Test reactive streams ok ...")
      println("Execution time: " + (tEnd - tStart) + "ms")
      system.terminate()
    }
  }

  def toAlchemyRequest(word: String): HttpRequest = {
    println(s"processing word: '${word}'")
    val url = s"$ALCHEMY_RESOURCE?apikey=$API_KEY&text=$word&outputMode=json"
    val h = HttpRequest(method = HttpMethods.GET, uri = url)
    return h
  }

}
